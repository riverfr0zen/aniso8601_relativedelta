===========
 aniso8601_relativedelta
===========

This fork essentially modifies the duration module so that it uses
relativedelta (from the dateutil package) instead of timedelta. If dateutil.relativedelta is not available, it will fallback to timedelta.

Usecases are wherever you may need to handle arithmetic for months & years so that the difference is that they are a whole number apart (rather than 60 days for months, and 365 days for a year).

For example, normally:

```
>>> datetime(2015, 5, 7) + parse_duration("P1M")
datetime.datetime(2015, 6, 6, 0, 0)
```

With this change, however:

```
>>> datetime(2015, 5, 7) + parse_duration("P1M")
datetime.datetime(2015, 6, 7, 0, 0)
```

This fork was created to satisfy some project requirements. The changes have not been tested thoroughly, nor its impact upon the rest of the aniso8601 package.